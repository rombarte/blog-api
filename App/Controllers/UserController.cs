using System.Linq;
using Application.Models;
using Application.Models.Requests;
using Application.Models.Response;
using Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace Application.Controllers
{
    public class UserController : Controller
    {
        private ApplicationDbContext DbContext;

        private ApiKeyService ApiKeyService;

        public UserController(ApplicationDbContext dbContext, ApiKeyService apiKeyService)
        {
            DbContext = dbContext;
            ApiKeyService = apiKeyService;
        }

        public ObjectResult CreateNewAccountAction([FromBody] UserRegister userRegister)
        {
            using (DbContext)
            {
                Auth auth = DbContext.Auth.Where<Auth>(m => m.Username == userRegister.Username).FirstOrDefault<Auth>();

                if (auth == null)
                {
                    auth = new Auth();
                    auth.Username = userRegister.Username;
                    auth.Password = userRegister.Password;

                    DbContext.Auth.Add(auth);
                    DbContext.SaveChanges();

                    return Ok(new DefaultMessage { Code = 0, Message = "User has been added" });
                }

                return BadRequest(new DefaultMessage { Code = 1, Message = "User exists" });
            }
            // @todo: Internal server error
        }

        public ObjectResult GenerateApiKeyAction([FromBody] UserRegister postData)
        {
            using (DbContext)
            {
                Auth auth = DbContext.Auth.Where<Auth>(m => m.Username == postData.Username).FirstOrDefault<Auth>();

                if (auth == null)
                {
                    return BadRequest(new DefaultMessage { Code = 2, Message = "User not exists" });
                }

                auth.ApiKey = ApiKeyService.CreateApiKey(auth.Username);

                DbContext.SaveChanges();

                return Ok(new UserAuthenticateSuccess { Code = 0, Message = "Success", ApiKey = auth.ApiKey });
            }

            // @todo: Internal server error
        }
    }
}