namespace Application.Models.Response
{
    public class DefaultMessage
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
