namespace Application.Models.Response
{
    public class UserAuthenticateSuccess : DefaultMessage
    {
        public string ApiKey { get; set; }
    }
}
