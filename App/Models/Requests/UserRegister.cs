namespace Application.Models.Requests
{
    public class UserRegister
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
