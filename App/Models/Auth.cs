using System.Text.Json.Serialization;

namespace Application.Models
{

    public class Auth
    {
        [JsonIgnore]
        public int Id { get; set; }

        public string Username { get; set; }
        public string ApiKey { get; set; }

        [JsonIgnore]
        public string Password { get; set; }
    }
}
