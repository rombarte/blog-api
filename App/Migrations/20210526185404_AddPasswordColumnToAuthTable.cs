﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Application.Migrations
{
    public partial class AddPasswordColumnToAuthTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "Auth",
                nullable: true
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Password",
                table: "Auth"
            );
        }
    }
}
