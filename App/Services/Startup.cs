using Application.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Application
{
    public class Startup
    {
        private IConfiguration Configuration;

        public Startup(IConfiguration config)
        {
            Configuration = config;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddControllersWithViews();

            services.AddScoped<ApiKeyService, ApiKeyService>();

            string connectionString = GetConnectionString(Configuration);

            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString));
        }

        private string GetConnectionString(IConfiguration config)
        {
            string host = config["Database:Host"];
            string database = config["Database:Database"];
            string user = config["Database:User"];
            string password = config["Database:Password"];

            return string.Format(
                "Data Source={0};Database={1};Integrated Security=false;User ID={2};Password={3};",
                host,
                database,
                user,
                password
            );
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "Authenticate",
                    pattern: "/v1/user/authenticate",
                    defaults: new { controller = "User", action = "GenerateApiKeyAction" }
                );

                endpoints.MapControllerRoute(
                    name: "Register",
                    pattern: "/v1/user/register",
                    defaults: new { controller = "User", action = "CreateNewAccountAction" }
                );
                
                //  endpoints.MapControllerRoute(
                //     name: "GetCategories",
                //     pattern: "/v1/categories/list",
                //     defaults: new { controller = "User", action = "GetCategoriesAction" }
                // );
            });

            UpgradeDatabase(app);
        }

        private void UpgradeDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                if (context != null && context.Database != null)
                {
                    context.Database.Migrate();
                }
            }
        }
    }
}