using System;
using System.Security.Cryptography;
using System.Text;

namespace Application.Services
{

    public class ApiKeyService
    {
        private SHA1 SHA1;

        public ApiKeyService()
        {
            SHA1 = System.Security.Cryptography.SHA1.Create();
        }

        public string CreateApiKey(string username)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(username);

            byte[] hash = SHA1.ComputeHash(bytes);

            return BitConverter.ToString(hash).Replace("-", "").ToLower();
        }
    }
}