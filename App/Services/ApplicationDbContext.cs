using Application.Models;
using Microsoft.EntityFrameworkCore;

namespace Application
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Auth> Auth { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }
    }
}