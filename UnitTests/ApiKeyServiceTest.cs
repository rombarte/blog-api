using Application.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StringLibraryTest
{
    [TestClass]
    public class ApiKeyServiceTest
    {
        [TestMethod]
        public void testCreateApiKeyReturnsCorrectHash()
        {
            var apiKeyService = new ApiKeyService();

            var result = apiKeyService.CreateApiKey("test");

            Assert.AreEqual("a94a8fe5ccb19ba61c4c0873d391e987982fbbd3", result);
        }
    }
}
