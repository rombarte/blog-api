# Blog API
## API written in ASP.NET Core 5.0

## Migrations
1. [Microsoft.EntityFrameworkCore](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore) is used for migrations.
2. Migrations are stored in **Migrations** directory.
3. For first time, create empty database:

    ```sh
    docker-compose exec db /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'Password1234' -Q 'CREATE DATABASE [Database]'
    ```

4. Add new migration by running Visual Studio Code task.
5. Migration will be applied before application restart.

## Tasks in Visual Studio Code

1. Run unit tests
2. Create migration

## License
All files in this repository are available under the MIT License. You can find more information and a link to the license content on the [Wikipedia](https://en.wikipedia.org/wiki/MIT_License) page.
